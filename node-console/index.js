'use strict'
const program = require('commander'),
    chalk = require("chalk"),
    exec = require('child_process').exec,
    pkg = require('./package.json')

let listFunction = (directory, options) => {
    const cmd = 'dir'
    let params = []
    if (options.all) params.push("a");
    if (options.long) params.push("l");

    let parametrizedCommand = params.length
        ? cmd + ' -' + params.join('')
        : cmd;
    if (directory) parametrizedCommand += ' ' + directory

    let output = (error, stdout, stderr) => {
        if (error) console.log(chalk.red.bold.underline("exec error:") + error)
        if (stdout) console.log(chalk.green.bold.underline("Result:") + stdout)
        if (stderr) console.log(chalk.red("Error: ") + stderr)
    }

    exec(parametrizedCommand, output)
}

program
    .version(pkg.version)
    .command('list [directory]')
    .description('List files and folders')
    .option('-a, --all', 'List all files and folders')
    .option('-l, --long', '')
    .action(listFunction)

program.parse(process.argv)

if (program.args.length === 0) program.help();